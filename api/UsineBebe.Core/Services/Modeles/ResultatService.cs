﻿namespace UsineBebe.Core.Services.Modeles
{
    /// <summary>
    /// Résultat d'un service
    /// </summary>
    public class ResultatService
    {
        private List<ErreurService> _erreurServices;

        /// <summary>
        /// Constructeur
        /// </summary>
        public ResultatService()
        {
            _erreurServices = new List<ErreurService>();
        }

        /// <summary>
        /// Constructeur
        /// </summary>
        /// <param name="erreurService">Erreur du service</param>
        public ResultatService(ErreurService erreurService)
        {
            _erreurServices = new List<ErreurService>() { erreurService };
        }

        /// <summary>
        /// Erreur du service
        /// </summary>
        public IEnumerable<ErreurService> ErreursService => _erreurServices;

        /// <summary>
        /// Indique si le service a fini en erreur
        /// </summary>
        public bool EstEnErreur => _erreurServices.Any();

        /// <summary>
        /// Ajoute une erreur 
        /// </summary>
        /// <param name="erreur">L'erreur à ajouter</param>
        public void AjouterErreur(ErreurService erreur)
        {
            _erreurServices.Add(erreur);
        }

        /// <summary>
        /// Ajoute une liste d'erreur
        /// </summary>
        /// <param name="erreurs">Les erreurs à ajouter</param>
        public void AjouterErreurs(IEnumerable<ErreurService> erreurs)
        {
            _erreurServices.AddRange(erreurs);
        }
    }

    /// <summary>
    /// Résultat d'un service retournant une valeur
    /// </summary>
    /// <typeparam name="T">Type de la valeur retournée</typeparam>
    public class ResultatService<T> : ResultatService
    {
        /// <summary>
        /// Valeur retournée par le service
        /// </summary>
        public T? Valeur { get; private set; }

        public void AjouterValeur(T valeur)
        {
            Valeur = valeur;
        }
    }
}
