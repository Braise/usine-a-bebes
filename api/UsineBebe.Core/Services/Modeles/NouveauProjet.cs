﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UsineBebe.Core.Services.Modeles
{
    /// <summary>
    /// Un nouveau projet à inscrire
    /// </summary>
    public class NouveauProjet
    {
        /// <summary>
        /// Constructeur
        /// </summary>
        /// <param name="nouveauxParticipants">Les nouveaux participants du nouveau projet</param>
        public NouveauProjet(IEnumerable<NouveauParticipant> nouveauxParticipants)
        {
            NouveauxParticipants = nouveauxParticipants;
        }

        /// <summary>
        /// Les nouveaux participants du nouveau projet
        /// </summary>
        public IEnumerable<NouveauParticipant> NouveauxParticipants { get; private set; }
    }
}
