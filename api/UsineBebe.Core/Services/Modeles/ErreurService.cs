﻿using System.ComponentModel;

namespace UsineBebe.Core.Services.Modeles
{
    /// <summary>
    /// Erreur pour les services
    /// </summary>
    public enum ErreurService
    {
        [Description("Individu inconnu")]
        IndividuNonTrouve = 1,

        [Description("Le projet contient des participants identiques")]
        ParticipantIdentique = 2,

        [Description("Le projet doit contenir au moins un participant avec une condition médicale")]
        AucuneConditionMedicale = 3,

        [Description("Le projet ne peut contenir un participant avec une stérilisation volontaire")]
        PresenceSterilisationVolontaire = 4,

        [Description("Un participant est déjà engagé dans un autre projet")]
        ParticipantDejaEngage = 5,

        [Description("Projet Inconnu")] 
        ProjetInconnu = 6,
    }
}
