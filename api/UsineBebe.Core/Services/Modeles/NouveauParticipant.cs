﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UsineBebe.Core.Services.Modeles
{
    /// <summary>
    /// Un nouveau participant à un projet
    /// </summary>
    public class NouveauParticipant
    {
        /// <summary>
        /// Constructeur
        /// </summary>
        /// <param name="Nam">Nam du participant</param>
        /// <param name="aDiagnostiqueInfertilite">Indique si a recu un diagnostique d'infertilité</param>
        /// <param name="aIncapaciteReproduire">Indique si une incapacité à reproduire existe</param>
        /// <param name="aSubitSterilisationVolontaire">Indique si a recu une stérilisation volontaire</param>
        public NouveauParticipant(string nam,
                                  bool aDiagnostiqueInfertilite,
                                  bool aIncapaciteReproduire,
                                  bool aSubitSterilisationVolontaire)
        {
            Nam = nam;
            ADiagnostiqueInfertilite = aDiagnostiqueInfertilite;
            AIncapaciteReproduire = aIncapaciteReproduire;
            ASterilisationVolontaire = aSubitSterilisationVolontaire;
        }

        /// <summary>
        /// Identifiant de l'individu du participant
        /// </summary>
        public string Nam { get; private set; }

        /// <summary>
        /// Indique si un diagnostique d'infertilité a été rendu
        /// </summary>
        public bool ADiagnostiqueInfertilite { get; private set; }

        /// <summary>
        /// Indique si une incapacité à se reproduire existe
        /// </summary>
        public bool AIncapaciteReproduire { get; private set; }

        /// <summary>
        /// Indique si le participant a subit une stérilisation volontaire
        /// </summary>
        public bool ASterilisationVolontaire { get; private set; }
    }
}
