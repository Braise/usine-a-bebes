﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UsineBebe.Core.Domaine;
using UsineBebe.Core.Services.Modeles;

namespace UsineBebe.Core.Services
{
    /// <summary>
    /// Services de l'individu
    /// </summary>
    public interface IIndividuService
    {
        /// <summary>
        /// Obtient l'individu associé au numéro d'assurance maladie
        /// </summary>
        /// <param name="nam">Numéro d'assurance maladie</param>
        /// <returns>Le résultat de l'opération contenant l'individu trouvé ou le code d'erreur</returns>
        ResultatService<Individu> ObtenirIndividu(string nam);
    }
}
