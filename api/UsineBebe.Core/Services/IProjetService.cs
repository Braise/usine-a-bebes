﻿using UsineBebe.Core.Domaine;
using UsineBebe.Core.Services.Modeles;

namespace UsineBebe.Core.Services
{
    /// <summary>
    /// Interface pour les services d'un projet
    /// </summary>
    public interface IProjetService
    {
        /// <summary>
        /// Crée un nouveau projet
        /// </summary>
        /// <param name="nouveauProjet">Le nouveau projet a créer</param>
        /// <returns>Le résultat de l'opération avec l'identifiant du projet créé si réussis</returns>
        Task<ResultatService<int>> Creer(NouveauProjet nouveauProjet);

        /// <summary>
        /// Recherche un projet sur base de son id
        /// </summary>
        /// <param name="id">Identifiant du projet</param>
        /// <returns>Le résultat de l'opération</returns>
        Task<ResultatService<Projet>> Rechercher(int id);
    }
}
