﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UsineBebe.Core.Domaine
{
    /// <summary>
    /// Un individu
    /// </summary>
    public class Individu
    {
        /// <summary>
        /// Constructeur
        /// </summary>
        /// <param name="id">Identifiant unique de l'individu</param>
        /// <param name="nom">Nom de famille</param>
        /// <param name="prenom">Prénom</param>
        /// <param name="dateNaissance">Date de naissance</param>
        /// <param name="sexe">sexe</param>
        /// <param name="nam">Numéro d'assurance sociale</param>
        /// <param name="estAdmissibileAssuranceMaladie">Indique si l'individu est admissible à l'assurance maladie</param>
        public Individu(int id,
                        string nom,
                        string prenom,
                        DateTime dateNaissance,
                        Sexe sexe,
                        string nam,
                        bool estAdmissibleAssuranceMaladie)
        {
            Id = id;
            Nom = nom;
            Prenom = prenom;
            DateNaissance = dateNaissance;
            Sexe = sexe;
            Nam = nam;
            EstAdmissibleAssuranceMaladie = estAdmissibleAssuranceMaladie;
        }

        /// <summary>
        /// Identifiant unique de l'individu
        /// </summary>
        public int Id { get; }

        /// <summary>
        /// Nom
        /// </summary>
        public string Nom { get; }

        /// <summary>
        /// Prenom
        /// </summary>
        public string Prenom { get; }

        /// <summary>
        /// Date de naissance
        /// </summary>
        public DateTime DateNaissance { get; }

        /// <summary>
        /// Sexe
        /// </summary>
        public Sexe Sexe { get; }

        /// <summary>
        /// Numéro d'assurance sociale
        /// </summary>
        public string Nam { get; }

        /// <summary>
        /// Indique si l'individu est admissible à l'assurance maladie
        /// </summary>
        public bool EstAdmissibleAssuranceMaladie { get; }
    }
}
