﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UsineBebe.Core.Services.Modeles;

namespace UsineBebe.Core.Domaine
{
    /// <summary>
    /// Un projet pour construire des bébés
    /// </summary>
    public class Projet
    {
        /// <summary>
        /// Constructeur
        /// </summary>
        /// <remarks>Private constructeur pour permettre l'utilisation d'ORM avec cette classe</remarks>
        private Projet()
        {
            
        }

        /// <summary>
        /// Constructeur
        /// </summary>
        /// <param name="participants">Participants au projet</param>
        /// <param name="dateCreation">Date de création du projet</param>
        public Projet(IEnumerable<Participant> participants,
                      DateTime dateCreation)
        {
            Participants = participants;
            DateCreation = dateCreation;
        }

        /// <summary>
        /// Constructeur
        /// </summary>
        /// <param name="id">Identifiant unique du projet</param>
        /// <param name="participants">Participants au projet</param>
        /// <param name="dateCreation">Date de création du projet</param>
        public Projet(int id,
                      IEnumerable<Participant> participants,
                      DateTime dateCreation) : this(participants, dateCreation)
        {
            Id = id;
        }

        /// <summary>
        /// Identifiant unique du projet
        /// </summary>
        public int Id { get; private set; }

        /// <summary>
        /// Participants au projet
        /// </summary>
        public IEnumerable<Participant> Participants { get; private set; }

        /// <summary>
        /// Date de création du projet
        /// </summary>
        public DateTime DateCreation { get; private set; }

        /// <summary>
        /// Détermine si un participant est présent plus de une fois dans le projet
        /// </summary>
        /// <returns></returns>
        public bool ContientParticipantIdentiques()
        {
            // Si les participants sont tous distinct alors il y a autant de id individu que de participants. Sinon on a au moins un doublon
            return Participants.DistinctBy(x => x.Individu.Id).Count() != Participants.Count();
        }

        /// <summary>
        /// Détermine si le projet contient au moins un participant avec une condition médicale
        /// </summary>
        /// <returns></returns>
        public bool ContientUneConditionMedicale()
        {
            return Participants.Any(x => x.ADiagnostiqueInfertilite || 
                                         x.AIncapaciteReproduire);
        }

        /// <summary>
        /// Détermine si le projet contient au moins un participant avec une stérilisation volontaire
        /// </summary>
        /// <returns></returns>
        public bool ContientSterilisationVolontaire()
        {
            return Participants.Any(x => x.ASterilisationVolontaire);
        }
    }
}
