﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UsineBebe.Core.Domaine
{
    /// <summary>
    /// Un participant à un projet
    /// </summary>
    public class Participant
    {
        /// <summary>
        /// Constructeur
        /// </summary>
        /// <remarks>Private constructeur pour permettre l'utilisation d'ORM avec cette classe</remarks>
        private Participant()
        {

        }

        /// <summary>
        /// Constructeur
        /// </summary>
        /// <param name="individu">Informations relatives à l'individu participant</param>
        /// <param name="aDiagnostiqueInfertilite">Indique si un diagnostique d'infertilité a été rendu</param>
        /// <param name="aIncapaciteReproduire">Indique si une incapacité à se reproduire existe</param>
        /// <param name="aSterilisationVolontaire">Indique si le participant a subit une stérilisation volontaire</param>
        public Participant(Individu individu,
                           bool aDiagnostiqueInfertilite,
                           bool aIncapaciteReproduire,
                           bool aSterilisationVolontaire)
        {
            Individu = individu;
            ADiagnostiqueInfertilite = aDiagnostiqueInfertilite;
            AIncapaciteReproduire = aIncapaciteReproduire;
            ASterilisationVolontaire = aSterilisationVolontaire;
        }

        /// <summary>
        /// Constructeur
        /// </summary>
        /// <param name="id">Identifiant unique du participant</param>
        /// <param name="individu">Informations relatives à l'individu participant</param>
        /// <param name="aDiagnostiqueInfertilite">Indique si un diagnostique d'infertilité a été rendu</param>
        /// <param name="aIncapaciteReproduire">Indique si une incapacité à se reproduire existe</param>
        /// <param name="aSterilisationVolontaire">Indique si le participant a subit une stérilisation volontaire</param>
        public Participant(int id,
                           Individu individu,
                           bool aDiagnostiqueInfertilite,
                           bool aIncapaciteReproduire,
                           bool aSterilisationVolontaire) : this(individu, aDiagnostiqueInfertilite, aIncapaciteReproduire, aSterilisationVolontaire)
        {
            Id = id;
        }

        /// <summary>
        /// Identifiant unique du participant
        /// </summary>
        public int Id { get; private set; }

        /// <summary>
        /// Informations relatives à l'individu participant
        /// </summary>
        public Individu Individu { get; private set; }

        /// <summary>
        /// Indique si un diagnostique d'infertilité a été rendu
        /// </summary>
        public bool ADiagnostiqueInfertilite { get; private set; }

        /// <summary>
        /// Indique si une incapacité à se reproduire existe
        /// </summary>
        public bool AIncapaciteReproduire { get; private set; }

        /// <summary>
        /// Indique si le participant a subit une stérilisation volontaire
        /// </summary>
        public bool ASterilisationVolontaire { get; private set; }
    }
}
