﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using UsineBebe.Persistance.Repositories;
using UsineBebe.Services.Repositories;

namespace UsineBebe.Persistance.Configurations
{
    public static class InjectionDependance
    {
        public static IServiceCollection AjouterPersistance(this IServiceCollection services, IConfiguration configuration)
        {
            var connectionString = configuration.GetConnectionString("DefaultConnection");

            services.AddScoped<IIndividuRepository, IndividuRepository>();
            services.AddScoped<IProjetRepository, ProjetRepository>();

            services.AddDbContext<UsineBebeDbContext>((sp, options) => 
            {
                options.UseSqlServer(connectionString);
            });

            services.AddScoped<ApplicationDbContextInitialisateur>();

            return services;
        }
    }
}
