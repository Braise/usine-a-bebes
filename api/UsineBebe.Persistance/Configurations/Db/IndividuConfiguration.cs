﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using UsineBebe.Core.Domaine;

namespace UsineBebe.Persistance.Configurations.Db
{
    /// <summary>
    /// Classe de configuration de l'Individu avec la BD
    /// </summary>
    internal class IndividuConfiguration : IEntityTypeConfiguration<Individu>
    {
        /// <summary>
        /// Configure le mapping Individu <=> BD
        /// </summary>
        /// <param name="builder">Le builder</param>
        public void Configure(EntityTypeBuilder<Individu> builder)
        {
            builder.ToTable("Individus");

            builder.HasKey(x => x.Id);

            builder.Property(p => p.Id)
                   .IsRequired()
                   .ValueGeneratedOnAdd();

            builder.Property(p => p.Nom)
                   .IsRequired()
                   .HasMaxLength(20);

            builder.Property(p => p.Prenom)
                   .IsRequired()
                   .HasMaxLength(20);

            builder.Property(p => p.DateNaissance)
                   .IsRequired();

            builder.Property(p => p.Sexe)
                    .IsRequired()
                    .HasConversion(
                        s => s.ToString(),
                        s => (Sexe)Enum.Parse(typeof(Sexe), s));

            builder.Property(p => p.Nam)
                   .IsRequired()
                   .HasMaxLength(12);

            builder.Property(p => p.EstAdmissibleAssuranceMaladie)
                    .IsRequired()
                    .HasConversion(
                        s => s ? 1 : 0,
                        s => Convert.ToBoolean(s));
        }
    }
}
