﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using UsineBebe.Core.Domaine;

namespace UsineBebe.Persistance.Configurations.Db
{
    public class ProjetConfiguration : IEntityTypeConfiguration<Projet>
    {
        public void Configure(EntityTypeBuilder<Projet> builder)
        {
            builder.ToTable("Projets");

            builder.HasKey(x => x.Id);

            builder.Property(p => p.Id)
                   .IsRequired()
                   .ValueGeneratedOnAdd();

            builder.Property(p => p.DateCreation)
                   .IsRequired();

            builder.HasMany(e => e.Participants)
                   .WithOne()
                   .IsRequired();
        }
    }
}
