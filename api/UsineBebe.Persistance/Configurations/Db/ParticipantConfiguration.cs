﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using UsineBebe.Core.Domaine;

namespace UsineBebe.Persistance.Configurations.Db
{
    public class ParticipantConfiguration : IEntityTypeConfiguration<Participant>
    {
        public void Configure(EntityTypeBuilder<Participant> builder)
        {
            builder.ToTable("Participants");

            builder.HasKey(p => p.Id);

            builder.Property(p => p.Id)
                   .IsRequired()
                   .ValueGeneratedOnAdd();

            builder.Property(p => p.ADiagnostiqueInfertilite)
                    .IsRequired()
                    .HasConversion(
                        s => s ? 1 : 0, 
                        s => Convert.ToBoolean(s));

            builder.Property(p => p.AIncapaciteReproduire)
                    .IsRequired()
                    .HasConversion(
                        s => s ? 1 : 0,
                        s => Convert.ToBoolean(s));

            builder.Property(p => p.ASterilisationVolontaire)
                    .IsRequired()
                    .HasConversion(
                        s => s ? 1 : 0,
                        s => Convert.ToBoolean(s));

            builder.HasOne(e => e.Individu)
                   .WithMany()
                   .HasForeignKey("IdIndividu")
                   .IsRequired();
        }
    }
}
