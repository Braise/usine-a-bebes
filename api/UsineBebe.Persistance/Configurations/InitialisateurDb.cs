﻿using Microsoft.Extensions.DependencyInjection;
using UsineBebe.Core.Domaine;
using Microsoft.AspNetCore.Builder;

namespace UsineBebe.Persistance.Configurations
{
    public static class InitialisateurDb
    {
        public static async Task InitialiserDb(this WebApplication webApp)
        {
            using var scope = webApp.Services.CreateScope();

            var initialiseur = scope.ServiceProvider.GetRequiredService<ApplicationDbContextInitialisateur>();

            await initialiseur.Seed();
        }
    }

    public class ApplicationDbContextInitialisateur
    {
        private readonly UsineBebeDbContext _context;

        public ApplicationDbContextInitialisateur(UsineBebeDbContext context)
        {
            _context = context;
        }

        public async Task Seed()
        {
            if (!_context.Individus.Any())
            {
                _context.Individus.Add(new Individu(default, "Dion", "Céline", new DateTime(1991, 1, 1), Sexe.Feminin, "DIOC91010115", true));
                _context.Individus.Add(new Individu(default, "Schwarzenegger", "Arnold", new DateTime(1992, 7, 30), Sexe.Masculin, "SCHA92073056", true));
                _context.Individus.Add(new Individu(default, "Stallone", "Sylvester", new DateTime(1989, 7, 6), Sexe.Masculin, "STAS89070687", false));
            }

            await _context.SaveChangesAsync();
        }
    }
}
