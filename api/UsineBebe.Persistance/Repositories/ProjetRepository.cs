﻿using Microsoft.EntityFrameworkCore;
using UsineBebe.Core.Domaine;
using UsineBebe.Services.Repositories;

namespace UsineBebe.Persistance.Repositories
{
    /// <summary>
    /// Classe d'implémentation du repository de projet
    /// </summary>
    public class ProjetRepository : IProjetRepository
    {
        private readonly UsineBebeDbContext _context;

        /// <summary>
        /// Constructeur
        /// </summary>
        /// <param name="context">Contexte</param>
        public ProjetRepository(UsineBebeDbContext context)
        {
            _context = context;
        }

        ///<inheritdoc/>
        public async Task EnregistrerNouveauProjet(Projet projet)
        {
            _context.Projets.Add(projet);

            await _context.SaveChangesAsync();
        }

        ///<inheritdoc/>
        public async Task<Projet?> Rechercher(int id)
        {
            return await _context.Projets.Include(x => x.Participants)
                                         .ThenInclude(x => x.Individu)
                                         .FirstOrDefaultAsync(x => x.Id == id);
        }

        ///<inheritdoc/>
        public async Task<bool> Existe(int idIndividu)
        {
            return await _context.Projets.AnyAsync(p => p.Participants.Any(pa => pa.Individu.Id == idIndividu));
        }
    }
}
