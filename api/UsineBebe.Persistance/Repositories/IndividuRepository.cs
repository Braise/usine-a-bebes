﻿using UsineBebe.Core.Domaine;
using UsineBebe.Services.Repositories;

namespace UsineBebe.Persistance.Repositories
{
    /// <summary>
    /// Repository de l'individu
    /// </summary>
    internal class IndividuRepository : IIndividuRepository
    {
        private readonly UsineBebeDbContext _dbContext;

        public IndividuRepository(UsineBebeDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public Individu? ObtenirIndividu(string nam)
        {
            return _dbContext.Individus.FirstOrDefault(x => x.Nam == nam);
        }
    }
}
