﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using UsineBebe.Core.Domaine;

namespace UsineBebe.Persistance
{
    /// <summary>
    /// Context d'accès à la base de données
    /// </summary>
    public class UsineBebeDbContext : DbContext
    {
        /// <summary>
        /// Db set des individus
        /// </summary>
        public DbSet<Individu> Individus => Set<Individu>();

        /// <summary>
        /// Db set des projets
        /// </summary>
        public DbSet<Projet> Projets => Set<Projet>();

        /// <summary>
        /// Db set des participants
        /// </summary>
        public DbSet<Participant> Participants => Set<Participant>();

        /// <summary>
        /// Constructeur
        /// </summary>
        /// <param name="options">Options de configuration du contexte</param>
        public UsineBebeDbContext(DbContextOptions<UsineBebeDbContext> options) : base(options) { }

        ///<inheritdoc/>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.LogTo(Console.WriteLine);
            base.OnConfiguring(optionsBuilder);
        }

        ///<inheritdoc/>
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());

            base.OnModelCreating(builder);
        }
    }
}
