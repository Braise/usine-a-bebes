﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace UsineBebe.Persistance.Data.Migrations
{
    /// <inheritdoc />
    public partial class AjoutAdmissibiliteAssuranceMaladie : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "EstAdmissibleAssuranceMaladie",
                table: "Individus",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EstAdmissibleAssuranceMaladie",
                table: "Individus");
        }
    }
}
