﻿using System.ComponentModel;

namespace UsineBebe.Utilitaire.Enums
{
    public static class Extension
    {
        public static string ObtenirDescription(this Enum source)
        {
            var typeEnum = source.GetType();
            var infoMembre = typeEnum.GetMember(source.ToString());

            if(infoMembre is not null && infoMembre.Any())
            {
                var attributs = infoMembre[0].GetCustomAttributes(typeof(DescriptionAttribute), false);
                if(attributs is not null && attributs.Any())
                {
                    return ((DescriptionAttribute)attributs[0]).Description;
                }
            }

            return source.ToString();
        }
    }
}
