﻿using UsineBebe.Core.Domaine;
using UsineBebe.Core.Services;
using UsineBebe.Core.Services.Modeles;
using UsineBebe.Services.Repositories;

namespace UsineBebe.Services
{
    /// <summary>
    /// Service pour un individu
    /// </summary>
    public class IndividuService : IIndividuService
    {
        private readonly IIndividuRepository _individuRepository;

        /// <summary>
        /// Constructeur
        /// </summary>
        public IndividuService(IIndividuRepository individuRepository)
        {
            _individuRepository = individuRepository;
        }

        ///<inheritdoc/>
        public ResultatService<Individu> ObtenirIndividu(string nam)
        {
            var resultat = new ResultatService<Individu>();
            var individu = _individuRepository.ObtenirIndividu(nam);

            if(individu is null)
            {
                resultat.AjouterErreur(ErreurService.IndividuNonTrouve);
            }
            else
            {
                resultat.AjouterValeur(individu);
            }

            return resultat;
        }
    }
}
