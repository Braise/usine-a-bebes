﻿using UsineBebe.Core.Domaine;
using UsineBebe.Core.Services;
using UsineBebe.Core.Services.Modeles;
using UsineBebe.Services.Repositories;

namespace UsineBebe.Services
{
    /// <summary>
    /// Implémentation des services d'un projet
    /// </summary>
    public class ProjetService : IProjetService
    {
        private readonly IIndividuService _individuService;
        private readonly IProjetRepository _projetRepository;

        /// <summary>
        /// Constructeur
        /// </summary>
        /// <param name="individuService">Services de l'individu</param>
        public ProjetService(IIndividuService individuService,
                            IProjetRepository projetRepository)
        {
            _individuService = individuService;
            _projetRepository = projetRepository;
        }

        ///<inheritdoc/>
        public async Task<ResultatService<int>> Creer(NouveauProjet nouveauProjet)
        {
            var resultat = new ResultatService<int>();
            var participants = new List<Participant>();

            foreach(var nouveauParticipant in nouveauProjet.NouveauxParticipants)
            {
                var resultatObtenirIndividu = _individuService.ObtenirIndividu(nouveauParticipant.Nam);

                if (resultat.EstEnErreur)
                {
                    resultat.AjouterErreurs(resultatObtenirIndividu.ErreursService);
                }
                else
                {
                    if (await _projetRepository.Existe(resultatObtenirIndividu.Valeur!.Id))
                    {
                        resultat.AjouterErreur(ErreurService.ParticipantDejaEngage);
                    }

                    participants.Add(new Participant(resultatObtenirIndividu.Valeur!,
                                                     nouveauParticipant.ADiagnostiqueInfertilite,
                                                     nouveauParticipant.AIncapaciteReproduire,
                                                     nouveauParticipant.ASterilisationVolontaire));
                }

            }

            var projet = new Projet(participants,
                                    DateTime.Now);

            if (projet.ContientParticipantIdentiques())
            {
                resultat.AjouterErreur(ErreurService.ParticipantIdentique);
            }

            if (!projet.ContientUneConditionMedicale())
            {
                resultat.AjouterErreur(ErreurService.ParticipantIdentique);
            }

            if (projet.ContientSterilisationVolontaire())
            {
                resultat.AjouterErreur(ErreurService.PresenceSterilisationVolontaire);
            }

            if (resultat.EstEnErreur)
            {
                return resultat;
            }

            await _projetRepository.EnregistrerNouveauProjet(projet);

            resultat.AjouterValeur(projet.Id);

            return resultat;
        }

        ///<inheritdoc/>
        public async Task<ResultatService<Projet>> Rechercher(int id)
        {
            var resultat = new ResultatService<Projet>();
            var projet = await _projetRepository.Rechercher(id);

            if(projet is null)
            {
                resultat.AjouterErreur(ErreurService.ProjetInconnu);
            }
            else
            {
                resultat.AjouterValeur(projet);
            }

            return resultat;
        }
    }
}
