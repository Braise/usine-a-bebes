﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UsineBebe.Core.Domaine;

namespace UsineBebe.Services.Repositories
{
    /// <summary>
    /// Interface du repository de l'individu
    /// </summary>
    public interface IIndividuRepository
    {
        /// <summary>
        /// Obtient un individu à partir de son numéro d'assurance maladie
        /// </summary>
        /// <param name="nam">Le numéro d'assurance maladie</param>
        /// <returns>L'individu correspondant</returns>
        Individu? ObtenirIndividu(string nam);
    }
}
