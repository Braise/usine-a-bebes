﻿using UsineBebe.Core.Domaine;

namespace UsineBebe.Services.Repositories
{
    /// <summary>
    /// Interface du repository de projet
    /// </summary>
    public interface IProjetRepository
    {
        /// <summary>
        /// Enregistre un nouveau projet en Bd
        /// </summary>
        /// <param name="projet">Le nouveau projet a enregistrer</param>
        /// <returns></returns>
        Task EnregistrerNouveauProjet(Projet projet);

        /// <summary>
        /// Recherche un projet sur base de son id
        /// </summary>
        /// <param name="id">l'Id du projet à rechercher</param>
        /// <returns>Le projet recherché</returns>
        Task<Projet?> Rechercher(int id);

        /// <summary>
        /// Détermine si un projet existe pour l'identifiant d'individu
        /// </summary>
        /// <param name="idIndividu">Identifiant individu à valider</param>
        /// <returns></returns>
        Task<bool> Existe(int idIndividu);
    }
}
