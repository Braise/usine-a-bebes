﻿using Microsoft.Extensions.DependencyInjection;
using UsineBebe.Core.Services;

namespace UsineBebe.Services.Configurations
{
    public static class InjectionDependance
    {
        public static IServiceCollection AjouterServices(this IServiceCollection services)
        {
            services.AddScoped<IIndividuService, IndividuService>();
            services.AddScoped<IProjetService, ProjetService>();

            return services;
        }
    }
}
