﻿using Microsoft.AspNetCore.Diagnostics;
using Microsoft.Extensions.Logging;

namespace UsineBebe.Api.GestionErreurs
{
    /// <summary>
    /// Gestionnaire d'erreurs imprévues
    /// </summary>
    public class GestionnaireErreurs : IExceptionHandler
    {
        private readonly ILogger<GestionnaireErreurs> _logger;

        /// <summary>
        /// Constructeur
        /// </summary>
        /// <param name="logger">Le loggeur</param>
        public GestionnaireErreurs(ILogger<GestionnaireErreurs> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// Journalise l'exception et retourne une erreur 500 à l'appelant
        /// </summary>
        /// <param name="httpContext">Context http</param>
        /// <param name="exception">Exception à journaliser</param>
        /// <param name="cancellationToken">Token de cancellation</param>
        /// <returns></returns>
        public ValueTask<bool> TryHandleAsync(HttpContext httpContext, Exception exception, CancellationToken cancellationToken)
        {
            _logger.LogError(exception, exception.Message);

            httpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;

            return ValueTask.FromResult(true);
        }

        private void LogException(Exception exception)
        {
            if(exception.InnerException is not null)
            {
                LogException(exception.InnerException);
            }

            
        }
    }
}
