﻿using Microsoft.AspNetCore.Mvc;
using UsineBebe.Api.DTOs;
using UsineBebe.Api.DTOs.Mappeurs;
using UsineBebe.Api.Retour;
using UsineBebe.Core.Services;
using UsineBebe.Core.Services.Modeles;
using UsineBebe.Utilitaire.Enums;

namespace UsineBebe.Api.Controllers
{
    /// <summary>
    /// Controller pour le projet
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class ProjetController : Controller
    {
        private readonly IProjetService _projetService;
        private readonly IMappeur _mappeur;

        /// <summary>
        /// Constructeur
        /// </summary>
        /// <param name="projetService">Services du projet</param>
        /// <param name="mappeur">Mappeur</param>
        public ProjetController(IProjetService projetService,
                                IMappeur mappeur)
        {
            _projetService = projetService;
            _mappeur = mappeur;
        }

        /// <summary>
        /// Créer un nouveau projet
        /// </summary>
        /// <param name="nouveauProjet">Les informations du nouveau projet</param>
        /// <returns>Id du projet créé</returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(Erreur))]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(int))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Creer(CreerProjetDto creerProjet)
        {
            var nouveauProjet = _mappeur.Map(creerProjet);

            var resultat = await _projetService.Creer(nouveauProjet);

            if (resultat.EstEnErreur)
            {
                var messages = resultat.ErreursService.Select(x => x.ObtenirDescription());
                return BadRequest(new Erreur(messages));
            }

            return Ok(resultat.Valeur);
        }

        /// <summary>
        /// Recherche un projet sur base de son identifiant
        /// </summary>
        /// <param name="id">Identifiant du projet</param>
        /// <returns>Résultat de l'opération</returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(Erreur))]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ProjetDto))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Rechercher(int id)
        {
            var resultat = await _projetService.Rechercher(id);

            if (resultat.EstEnErreur && resultat.ErreursService.Contains(ErreurService.ProjetInconnu))
            {
                return NotFound();
            }
            else if(resultat.EstEnErreur)
            {
                var messages = resultat.ErreursService.Select(x => x.ObtenirDescription());
                return BadRequest(new Erreur(messages));
            }

            return Ok(_mappeur.Map(resultat.Valeur!));
        }
    }
}
