﻿using Microsoft.AspNetCore.Mvc;
using UsineBebe.Api.DTOs;
using UsineBebe.Api.DTOs.Mappeurs;
using UsineBebe.Api.Retour;
using UsineBebe.Core.Services;
using UsineBebe.Core.Services.Modeles;

namespace UsineBebe.Api.Controllers
{
    /// <summary>
    /// Controller pour l'individu
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class IndividuController : ControllerBase
    {
        private readonly IIndividuService _individuService;
        private readonly IMappeur _mappeur;

        /// <summary>
        /// Constructeur
        /// </summary>
        /// <param name="individuService">Services de l'individu</param>
        public IndividuController(IIndividuService individuService,
                                  IMappeur mappeur)
        {
            _individuService = individuService;
            _mappeur = mappeur;
        }

        /// <summary>
        /// Obtenir une individu sur base de son numéro d'assurance sociale
        /// </summary>
        /// <param name="nam">Numéro d'assurance sociale</param>
        /// <returns>Le résultat de l'opération</returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IndividuDto))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult ObtenirIndividu(string nam)
        {
            var resultat = _individuService.ObtenirIndividu(nam);

            if(resultat.EstEnErreur && resultat.ErreursService.Contains(ErreurService.IndividuNonTrouve))
            {
                return NotFound();
            }

            return Ok(_mappeur.Map(resultat.Valeur!));
        }
    }
}
