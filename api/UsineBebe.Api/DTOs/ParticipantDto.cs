﻿namespace UsineBebe.Api.DTOs
{
    /// <summary>
    /// Dto du participant
    /// </summary>
    public class ParticipantDto
    {
        /// <summary>
        /// Id du participant
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Information de l'individu
        /// </summary>
        public IndividuDto Individu { get; set; }

        /// <summary>
        /// A eu une stérilisation volontaire
        /// </summary>
        public bool ASterilisationVolontaire { get; set; }

        /// <summary>
        /// Indique si une incapacité à se reproduire existe
        /// </summary>
        public bool AIncapaciteReproduire { get; set; }

        /// <summary>
        /// A recu un diagnostique d'infertilité
        /// </summary>
        public bool ARecuDiagnostiqueInfertilite { get; set; }
    }
}
