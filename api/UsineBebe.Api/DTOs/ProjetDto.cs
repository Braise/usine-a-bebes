﻿namespace UsineBebe.Api.DTOs
{
    /// <summary>
    /// Dto du projet
    /// </summary>
    public class ProjetDto
    {
        /// <summary>
        /// Participants au projet
        /// </summary>
        public IEnumerable<ParticipantDto> Participants { get; set; }

        /// <summary>
        /// Date de création du projet
        /// </summary>
        public DateTime DateCreation { get; set; }
    }
}
