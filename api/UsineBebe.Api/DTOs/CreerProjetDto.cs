﻿namespace UsineBebe.Api.DTOs
{
    /// <summary>
    /// Dto pour la création d'un nouveau projet
    /// </summary>
    public class CreerProjetDto
    {
        /// <summary>
        /// Les participants constituants le projet
        /// </summary>
        public IEnumerable<NouveauParticipantDto> Participants { get; set; }
    }
}
