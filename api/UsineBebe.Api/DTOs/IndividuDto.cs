﻿using UsineBebe.Core.Domaine;

namespace UsineBebe.Api.DTOs
{
    public class IndividuDto
    {
        /// <summary>
        /// Nom
        /// </summary>
        public string Nom { get; set; }

        /// <summary>
        /// Prenom
        /// </summary>
        public string Prenom { get; set; }

        /// <summary>
        /// Date de naissance
        /// </summary>
        public DateTime DateNaissance { get; set; }

        /// <summary>
        /// Sexe
        /// </summary>
        public string Sexe { get; set; }

        /// <summary>
        /// Numéro d'assurance sociale
        /// </summary>
        public string Nam { get; set; }

        /// <summary>
        /// Indique si l'individu est admissible à l'assurance maladie
        /// </summary>
        public bool EstAdmissibleAssuranceMaladie { get; set; }
    }
}
