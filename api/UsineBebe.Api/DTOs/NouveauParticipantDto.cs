﻿namespace UsineBebe.Api.DTOs
{
    /// <summary>
    /// Un nouveau participant à un projet
    /// </summary>
    public class NouveauParticipantDto
    {
        /// <summary>
        /// Nam du participant
        /// </summary>
        public string Nam { get; set; }

        /// <summary>
        /// A eu une stérilisation volontaire
        /// </summary>
        public bool ASterilisationVolontaire { get; set; }

        /// <summary>
        /// Indique si une incapacité à se reproduire existe
        /// </summary>
        public bool AIncapaciteReproduire { get; set; }

        /// <summary>
        /// A recu un diagnostique d'infertilité
        /// </summary>
        public bool ARecuDiagnostiqueInfertilite { get; set; }
    }
}
