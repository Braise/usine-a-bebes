﻿using UsineBebe.Core.Domaine;
using UsineBebe.Core.Services.Modeles;

namespace UsineBebe.Api.DTOs.Mappeurs
{
    /// <summary>
    /// Interface de mapping
    /// </summary>
    public interface IMappeur
    {
        /// <summary>
        /// Map l'individu du domaine vers le dto
        /// </summary>
        /// <param name="individu">L'individu du domaine</param>
        /// <returns>Le dto</returns>
        IndividuDto Map(Individu individu);

        /// <summary>
        /// Map le dto de création de projet vers un nouveau projet
        /// </summary>
        /// <param name="creerProjetDto">Dto de création de projet</param>
        /// <returns>un Nouveau projet</returns>
        NouveauProjet Map(CreerProjetDto creerProjetDto);

        /// <summary>
        /// Map le projet du domaine vers un dto
        /// </summary>
        /// <param name="projet">Le projet du domaine</param>
        /// <returns>Le dto</returns>
        ProjetDto Map(Projet projet);
    }
}
