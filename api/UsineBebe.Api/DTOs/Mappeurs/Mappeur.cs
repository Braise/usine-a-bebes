﻿using UsineBebe.Core.Domaine;
using UsineBebe.Core.Services.Modeles;

namespace UsineBebe.Api.DTOs.Mappeurs
{
    /// <summary>
    /// Implémentation des mappings
    /// </summary>
    public class Mappeur : IMappeur
    {
        ///<inheritdoc/>
        public IndividuDto Map(Individu individu)
        {
            return new IndividuDto()
            {
                Nom = individu.Nom,
                Prenom = individu.Prenom,
                DateNaissance = individu.DateNaissance,
                Sexe = individu.Sexe.ToString(),
                Nam = individu.Nam,
                EstAdmissibleAssuranceMaladie = individu.EstAdmissibleAssuranceMaladie
            };
        }

        ///<inheritdoc/>
        public NouveauProjet Map(CreerProjetDto creerProjetDto)
        {
            var nouveauxParticipants = new List<NouveauParticipant>();

            foreach(var participant in creerProjetDto.Participants)
            {
                nouveauxParticipants.Add(new NouveauParticipant(participant.Nam,
                                                                participant.ARecuDiagnostiqueInfertilite,
                                                                participant.AIncapaciteReproduire,
                                                                participant.ASterilisationVolontaire));
            }

            return new NouveauProjet(nouveauxParticipants);
        }

        ///<inheritdoc/>
        public ProjetDto Map(Projet projet)
        {
            var participants = new List<ParticipantDto>();

            foreach (var participant in projet.Participants)
            {
                participants.Add(new ParticipantDto()
                {
                    Id = participant.Id,
                    Individu = Map(participant.Individu),
                    ARecuDiagnostiqueInfertilite = participant.ADiagnostiqueInfertilite,
                    AIncapaciteReproduire = participant.AIncapaciteReproduire,
                    ASterilisationVolontaire = participant.ASterilisationVolontaire
                });
            }

            return new ProjetDto()
            {
                Participants = participants,
                DateCreation = projet.DateCreation
            };
        }
    }
}
