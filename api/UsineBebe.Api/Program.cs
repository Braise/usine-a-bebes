
using Serilog;
using UsineBebe.Api.DTOs.Mappeurs;
using UsineBebe.Api.GestionErreurs;
using UsineBebe.Persistance.Configurations;
using UsineBebe.Services.Configurations;

namespace UsineBebe.Api
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // configurer le logging
            Log.Logger = new LoggerConfiguration()
                .WriteTo.Console()
                .MinimumLevel.Warning()
                .CreateLogger();

            // Add services to the container.
            builder.Services.AddExceptionHandler<GestionnaireErreurs>();
            builder.Services.AjouterPersistance(builder.Configuration);
            builder.Services.AjouterServices();
            builder.Services.AddScoped<IMappeur, Mappeur>();

            builder.Services.AddCors(options => 
            {
                options.AddPolicy(name: "localhost", policy => { policy.WithOrigins("http://localhost").AllowAnyHeader().AllowAnyOrigin(); });
            });

            builder.Services.AddControllers();
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();

            var app = builder.Build();

            app.UseExceptionHandler(_ => { });

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                await app.InitialiserDb();
            }

            app.UseSwagger();
            app.UseSwaggerUI();

            //app.UseHttpsRedirection();

            app.UseCors("localhost");

            app.UseAuthorization();


            app.MapControllers();

            app.Run();
        }
    }
}
