﻿namespace UsineBebe.Api.Retour
{
    /// <summary>
    /// Indication sur l'erreur à retourner à l'appelant
    /// </summary>
    public class Erreur
    {
        public Erreur(string message)
        {
            Messages = new List<string>() { message };
        }

        /// <summary>
        /// Constructeur
        /// </summary>
        /// <param name="messages">Messages d'erreur</param>
        public Erreur(IEnumerable<string> messages)
        {
            Messages = messages;
        }

        /// <summary>
        /// Messages d'erreurs
        /// </summary>
        public IEnumerable<string> Messages { get; set; }
    }
}
