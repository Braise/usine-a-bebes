import { Injectable } from "@angular/core";
import { Participant } from "../modeles/participant";

@Injectable({
    providedIn: 'root',
})
export class NouveauProjetParametreService{
    participants: Array<Participant> = [];
}