import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs/internal/Observable";
import { environment } from "../../environments/environment";
import { CreerProjetDto } from "../modeles/creerProjetDto";
import { Projet } from "../modeles/projet";

@Injectable({
    providedIn: 'root'
})
export class ProjetServiceHttp {

    private projetUrl = `${environment.apiUrl}/projet`

    constructor(private httpClient: HttpClient) {
        
    }

    public creerProjet(dto: CreerProjetDto): Observable<number>{
        return this.httpClient
            .post<number>(`${this.projetUrl}`, dto);
    }

    public obtenirProjet(id: number): Observable<Projet>{
        return this.httpClient
            .get<Projet>(`${this.projetUrl}?id=${id}`);
    }
}