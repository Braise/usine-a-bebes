import { Injectable } from '@angular/core';
import { EMPTY, Observable, catchError, empty, of, throwError } from 'rxjs';
import { Individu } from '../modeles/individu';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class IndividuServiceHttp{

    private individuUrl = `${environment.apiUrl}/individu`

    constructor(private httpClient: HttpClient) {
        
    }

    obtenirIndividu(nam: string): Observable<Individu> {
        return this.httpClient
          .get<Individu>(`${this.individuUrl}?nam=${nam}`)
          .pipe(catchError(this.handleError));
    }
  
  private handleError(error: HttpErrorResponse): Observable<never>{
    if (error.status === 404) {
          return throwError(() => error);
    }
    
      console.log(`Une erreur imprévue est survenue : ${error.message}`);
      return throwError(() => error);
    }
 }