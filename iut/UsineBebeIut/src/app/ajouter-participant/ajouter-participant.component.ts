import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Participant } from '../modeles/participant';
import { Individu } from '../modeles/individu';
import { CommonModule } from '@angular/common';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { RechercheNamComponent } from '../rechercher-nam/rechercher-nam.component';
import { Router } from '@angular/router';
import { NouveauProjetParametreService } from '../services/nouveau-projet-parametre.service';

@Component({
  selector: 'usine-ajouter-participant',
  standalone: true,
  imports: [CommonModule,  ReactiveFormsModule, RechercheNamComponent],
  templateUrl: './ajouter-participant.component.html',
  styleUrl: './ajouter-participant.component.css'
})
export class AjouterParticipantComponent {

  individu?: Individu;
  conditionsMedicales = new FormGroup({
    aDiagnostiqueInfertilite: new FormControl(false),
    aIncapaciteReproduire: new FormControl(false),
    aSubitSterilisationVolontaire : new FormControl(false)
  });
  
  constructor(private router: Router,
              private nouveauProjet: NouveauProjetParametreService) {

  }

  obtenirIndividu(individu: Individu) {
    this.individu = individu;
  }

  onAjoutParticipant() {
    let participant = new Participant(this.individu!,
      0,
      this.conditionsMedicales.value.aSubitSterilisationVolontaire!,
      this.conditionsMedicales.value.aIncapaciteReproduire!,
      this.conditionsMedicales.value.aDiagnostiqueInfertilite!);
    this.nouveauProjet.participants.push(participant);
    this.router.navigate(['/nouveau-projet']);
  }
}
