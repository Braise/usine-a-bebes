import { Component } from '@angular/core';
import { ProjetServiceHttp } from '../services/projet.service-http';
import { ActivatedRoute } from '@angular/router';
import { Projet } from '../modeles/projet';
import { CommonModule } from '@angular/common';
import { ConsulterParticipantsComponent } from '../consulter-participants/consulter-participants.component';

@Component({
  selector: 'usine-gerer-projet',
  standalone: true,
  imports: [CommonModule, ConsulterParticipantsComponent],
  templateUrl: './gerer-projet.component.html',
  styleUrl: './gerer-projet.component.css'
})
export class GererProjetComponent {

  projet?: Projet;

  constructor(
    private route: ActivatedRoute,
    private projetService: ProjetServiceHttp) {
    
  }

  ngOnInit(): void {
    let projetId = Number(this.route.snapshot.paramMap.get('id'));
    this.projetService
      .obtenirProjet(projetId)
      .subscribe({
        next: value => { this.projet = value }
      });
  }
}
