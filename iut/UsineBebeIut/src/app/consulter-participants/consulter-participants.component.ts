import { Component, Input } from '@angular/core';
import { Participant } from '../modeles/participant';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'usine-consulter-participants',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './consulter-participants.component.html',
  styleUrl: './consulter-participants.component.css'
})
export class ConsulterParticipantsComponent {
  @Input()
  participant!: Participant
}
