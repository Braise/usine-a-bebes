import { Routes } from '@angular/router';
import { AjouterParticipantComponent } from './ajouter-participant/ajouter-participant.component';
import { CreerProjetComponent } from './creer-projet/creer-projet.component';
import { GererProjetComponent } from './gerer-projet/gerer-projet.component';

export const routes: Routes = [
    { path: '', redirectTo: 'nouveau-projet', pathMatch: 'full' }, // route par défaut
    { path: 'nouveau-projet', component: CreerProjetComponent },
    { path: 'ajouter-participant', component: AjouterParticipantComponent },
    { path: 'gerer-projet/:id', component: GererProjetComponent}
];
