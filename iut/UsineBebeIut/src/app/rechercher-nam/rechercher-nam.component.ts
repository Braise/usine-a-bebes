import { Component, EventEmitter, Output } from '@angular/core';
import { FormControl, ReactiveFormsModule, Validators } from '@angular/forms';
import { IndividuServiceHttp } from '../services/individu.service-http';
import { Individu } from '../modeles/individu';
import { HttpErrorResponse, HttpStatusCode } from '@angular/common/http';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'usine-rechercher-nam',
  standalone: true,
  imports: [ReactiveFormsModule, CommonModule],
  templateUrl: './rechercher-nam.component.html',
  styleUrl: './rechercher-nam.component.css'
})
export class RechercheNamComponent {

  @Output()
  individuEventEmitter = new EventEmitter<Individu>();

  nam = new FormControl('', [Validators.minLength(12)]);
  estInconnu: boolean = false;

  constructor(private individuServiceHttp: IndividuServiceHttp) { }
  
  rechercherNam() {
    this.estInconnu = false;
    this.individuServiceHttp.obtenirIndividu(this.nam.value!)
      .subscribe({
        next: value => {
          this.individuEventEmitter.emit(value);
        },
        error: (error: HttpErrorResponse) => {
          if (error.status === HttpStatusCode.NotFound) {
            this.estInconnu = true;
          }
        }
      });
  }
}
