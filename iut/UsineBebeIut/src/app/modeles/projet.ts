import { Participant } from "./participant";

export interface Projet{
    participants: Array<Participant>,
    dateCreation: Date
}