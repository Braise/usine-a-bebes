import { Individu } from "./individu"

export class Participant {
    constructor(
        public individu: Individu,
        public id: number,
        public ASterilisationVolontaire: boolean,
        public aIncapaciteReproduire: boolean,
        public aRecuDiagnostiqueInfertilite: boolean
    ) { }
}