import { NouveauParticipantDto } from "./nouveauParticipantDto";

export interface CreerProjetDto{
    participants: Array<NouveauParticipantDto>
}