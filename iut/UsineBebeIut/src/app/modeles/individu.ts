export interface Individu{
    id: number,
    prenom: string,
    nom: string,
    nam: any,
    dateNaissance: Date,
    estAdmissibleAssuranceMaladie: boolean
}