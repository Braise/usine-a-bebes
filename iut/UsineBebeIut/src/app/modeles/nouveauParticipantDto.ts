export interface NouveauParticipantDto{
    nam: string,
    aSterilisationVolontaire: boolean,
    aIncapaciteReproduire: boolean,
    aRecuDiagnostiqueInfertilite: boolean
}