import { Component } from '@angular/core';
import { AjouterParticipantComponent } from '../ajouter-participant/ajouter-participant.component';
import { Individu } from '../modeles/individu';
import { RechercheNamComponent } from '../rechercher-nam/rechercher-nam.component';
import { Participant } from '../modeles/participant';
import { CommonModule } from '@angular/common';
import { Router, RouterModule } from '@angular/router';
import { NouveauProjetParametreService } from '../services/nouveau-projet-parametre.service';
import { ProjetServiceHttp } from '../services/projet.service-http';
import { NouveauParticipantDto } from '../modeles/nouveauParticipantDto';
import { CreerProjetDto } from '../modeles/creerProjetDto';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'usine-creer-projet',
  standalone: true,
  imports: [AjouterParticipantComponent, CommonModule, RouterModule],
  templateUrl: './creer-projet.component.html',
  styleUrl: './creer-projet.component.css'
})
export class CreerProjetComponent {

  messagesErreurs: Array<string> = new Array<string>();

  constructor(
    public nouveauProjet: NouveauProjetParametreService,
    private projetService: ProjetServiceHttp,
    private router: Router) {
    
  }

  surCreerProjetClick() {
    this.messagesErreurs.length = 0;
    let nouveauxParticipants = new Array<NouveauParticipantDto>();
    
    this.nouveauProjet.participants.forEach(element => {
      let nouveauParticipantDto: NouveauParticipantDto =
      {
        nam: element.individu.nam,
        aSterilisationVolontaire: element.ASterilisationVolontaire,
        aIncapaciteReproduire: element.aIncapaciteReproduire,
        aRecuDiagnostiqueInfertilite: element.aRecuDiagnostiqueInfertilite
      }
      
      nouveauxParticipants.push(nouveauParticipantDto);
    });

    let creerProjet: CreerProjetDto = { participants: nouveauxParticipants };

    this.projetService
      .creerProjet(creerProjet)
      .subscribe({
        next: value => {
          this.router.navigate(['/gerer-projet/' + value]);
        },
        error: (error: HttpErrorResponse) => {
          if (error.status == 400) {
            console.log(error.error.messages);
            this.messagesErreurs.push(error.error.messages);
          }
        }
      });
  }
}
